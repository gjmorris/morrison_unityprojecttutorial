﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainShipController : MonoBehaviour {

    public float moveSpeed;
    public float rotationSpeed;

    private Rigidbody rb;

    public Text TotalGoldText;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        transform.position = GameInfo.playerPos;
        transform.rotation = GameInfo.playerRotation;
    }

    private void Update() {
        TotalGoldText.text = GameInfo.totalGold.ToString();
    }

    void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis("Horizontal");//A and D key
        float moveVertical = Input.GetAxis("Vertical");//W and S key

        rb.angularVelocity += rotationSpeed * new Vector3(0, moveHorizontal, 0);
        rb.velocity += moveSpeed * (transform.forward * moveVertical);

        /*transform.rotation = Quaternion.Lerp(transform.localRotation, new Quaternion(0f, transform.localRotation.y, 0f, 0f), Time.deltaTime * 2f);*/
    }
}
