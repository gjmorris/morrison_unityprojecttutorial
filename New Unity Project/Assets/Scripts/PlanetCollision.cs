﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlanetCollision : MonoBehaviour {

    float gold;
    public float decay;
    public TextMesh goldText;
    public string color;
    public Text winText;
    public GameObject Menu;
    float timePassed = 0;

    void Start() {
        //Red (1,0,0,1)
        //Green (0,1,0,1)

        if (color == "red") {
            gold = GameInfo.redGold;
        }
        else if (color == "blue") {
            gold = GameInfo.blueGold;
        }
        else if (color == "green") {
            gold = GameInfo.greenGold;
        }
        else if (color == "yellow") {
            gold = GameInfo.yellowGold;
        }
        else if (color == "pink") {
            gold = GameInfo.pinkGold;
        }

        goldText.text = gold.ToString();
        goldText.color = new Color(0,1,0,1);

        if (GameInfo.checkWin()) {
            winText.text = "Game Over!";
        }
        else {
            winText.text = "";
        }

    }

    private void Update() {
        timePassed += Time.deltaTime;

        if (timePassed >= 1f) {
            gold -= 1 + (1 * decay);

            if (color == "red") {
                GameInfo.redGold = gold;
            }
            else if (color == "blue") {
                GameInfo.blueGold = gold;
            }
            else if (color == "green") {
                GameInfo.greenGold = gold;
            }
            else if (color == "yellow") {
                GameInfo.yellowGold = gold;
            }
            else if (color == "pink") {
                GameInfo.pinkGold = gold;
            }

            timePassed = 0f;
        }

        if (gold < 0) {
            goldText.color = new Color(1 , 0 , 0 , 1);
            if (color == "red") {
                GameInfo.red = true;
            }
            else if (color == "blue") {
                GameInfo.blue = true;
            }
            else if (color == "green") {
                GameInfo.green = true;
            }
            else if (color == "yellow") {
                GameInfo.yellow = true;
            }
            else if (color == "pink") {
                GameInfo.pink = true;
            }

            if (color == "red" && GameInfo.red) {
                goldText.color = new Color(1 , 0 , 0 , 1);
            }
            else if (color == "blue" && GameInfo.blue) {
                goldText.color = new Color(1 , 0 , 0 , 1);
            }
            else if (color == "green" && GameInfo.green) {
                goldText.color = new Color(1 , 0 , 0 , 1);
            }
            else if (color == "yellow" && GameInfo.yellow) {
                goldText.color = new Color(1 , 0 , 0 , 1);
            }
            else if (color == "pink" && GameInfo.pink) {
                goldText.color = new Color(1 , 0 , 0 , 1);
            }

            goldText.text = "X";

        }
        else {
            goldText.text = gold.ToString();
        }
    }

    private void OnCollisionEnter(Collision collision) {

        if (collision.gameObject.CompareTag("Player")){

            if (GameInfo.finished)
            {
                
                GameInfo.planet = color;
                GameInfo.playerPos = collision.gameObject.transform.position;
                GameInfo.playerRotation = collision.gameObject.transform.rotation;
                GameInfo.finished = false;
                Menu.SetActive(true);
                //SceneManager.LoadScene("MainGame");
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {

        GameInfo.finished = true;
        Menu.SetActive(false);

    }
}
