﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;
    public GameObject gold;
    public int scoreValue;
    private GameController gameController;

    private void Start() {

        GameObject gameControllerObj = GameObject.FindWithTag("GameController");

        if (gameControllerObj != null) {
            gameController = gameControllerObj.GetComponent<GameController>();
        }
        else {
            Debug.Log("Could not find gameControllerObj");
        }

    }

    private void OnTriggerEnter(Collider other) {

        if (other.tag == "Boundary") {
            return;
        }

        Instantiate(explosion, transform.position, transform.rotation);

        if (other.tag == "Player") {
            Instantiate(playerExplosion , other.transform.position , other.transform.rotation);
            gameController.GameOver(false);
        }

        //gameController.AddScore(scoreValue);
        StartCoroutine(SpawnGold(other));
        
        Destroy(other.gameObject);
        Destroy(gameObject);

    }

    IEnumerator SpawnGold(Collider other) {
        int stopCount = Random.Range(1,5);
        int count = 0;

        while (count < stopCount) {
            Instantiate(gold , other.transform.position , other.transform.rotation);
            yield return new WaitForSeconds(0.1f);
            count++;
        }

    }

}
