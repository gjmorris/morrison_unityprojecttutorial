﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public GameObject[] hazards;
    public Vector3 spawnValues;
    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    private int score;
    private int requiredScore;

    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    private bool gameOver;
    private bool restart;

    public static GameController gc;

    private void Start() {
        gc = this;
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        requiredScore = GameInfo.getGoldReqirement();
        UpdateScore();
        StartCoroutine(SpawnWaves());

    }

    private void Update() {

        if (restart) {
            if (Input.GetKeyDown(KeyCode.R)) {
                //Application.LoadLevel(Application.loadedLevel);
                SceneManager.LoadScene("FleetScene");
            }
        }

    }

    IEnumerator SpawnWaves() {

        yield return new WaitForSeconds(startWait);

        while (true) {

            if (score >= requiredScore) {
                GameOver(true);
            }

            if (gameOver) {
                restartText.text = "Press 'R' to return to fleet view";
                restart = true;
                break;
            }

            for (int i = 0; i < hazardCount; i++) {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];

                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x , spawnValues.x) , spawnValues.y , spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;

                Instantiate(hazard , spawnPosition , spawnRotation);

                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
 
        }
    }

    public void AddScore(int addScore) {
        score += addScore;
        UpdateScore();
    }

    void UpdateScore() {
        string temp = "Gold: " + score + " / " + requiredScore;
        scoreText.text = temp;
            
    }

    public void GameOver(bool win) {
        if (win) {
            gameOverText.text = "Mission Complete";
            
            GameInfo.totalGold += score * 3;
        }
        else {
            gameOverText.text = "Ship Destroyed!";
            GameInfo.totalGold += score;
        }
        gameOver = true;

    }
}
