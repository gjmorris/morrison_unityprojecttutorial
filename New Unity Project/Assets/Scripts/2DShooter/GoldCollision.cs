﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldCollision : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {

        if (other.tag == "Player") {

            GameController.gc.AddScore(Random.Range(1,4));
            Destroy(gameObject);
        }

    }
}
