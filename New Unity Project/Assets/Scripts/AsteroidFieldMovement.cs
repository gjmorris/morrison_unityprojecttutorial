﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidFieldMovement : MonoBehaviour {

    private Rigidbody rb;
    float lifetime;

    private void Start() {

        lifetime = 1f;
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * 3f;

    }

    private void Update() {

        lifetime += Time.deltaTime;

        if (lifetime > 43f) {
            Destroy(gameObject);
        }

    }
}
