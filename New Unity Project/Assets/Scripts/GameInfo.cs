﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameInfo {

    public static float totalGold;

    public static bool blue, red, green, yellow, pink = false;

    public static float blueGold = 100, redGold = 100, greenGold = 100, yellowGold = 100, pinkGold = 100;

    public static string planet;

    public static Vector3 playerPos = new Vector3(2.18f, 7.49f, -21.66f);
    public static Quaternion playerRotation = new Quaternion();
    public static bool finished = true;

    public static int getGoldReqirement() {
        return Random.Range(5, 30) * 10;
    }

    public static void setBlueTrue() {
        blue = true;
    }

    public static void setBlueFalse() {
        blue = false;
    }

    public static void setRedTrue() {
        red = true;
    }

    public static void setRedFalse() {
        red = false;
    }

    public static void setGreenTrue() {
        green = true;
    }

    public static void setGreenFalse() {
        green = false;
    }

    public static void setYellowTrue() {
        yellow = true;
    }

    public static void setYellowFalse() {
        yellow = false;
    }

    public static void setPinkTrue() {
        pink = true;
    }

    public static void setPinkFalse() {
        pink = false;
    }

    public static bool checkWin() {
        if (blue && red && green && yellow && pink) {
            return true;
        }
        return false;
    }

}
