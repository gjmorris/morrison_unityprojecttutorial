﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour {

    public GameObject asteroidField;

    private void Start() {

        StartCoroutine("Spawn");

    }


    IEnumerator Spawn() {

        while (true) {

            float zDiff = Random.Range(-15f, 70f);

            Instantiate(asteroidField , new Vector3(transform.position.x , transform.position.y , transform.position.z + zDiff) , transform.rotation);

            yield return new WaitForSeconds(2f);
        }

    }

}
