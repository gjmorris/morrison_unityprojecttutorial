﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpMenu : MonoBehaviour {

    public Button BtnFiftyPercent, BtnTwentyFivePercent, BtnSeventyFivePercent;

    public GameObject Menu;

	// Use this for initialization
	void Start () {

        BtnFiftyPercent.onClick.AddListener(FiftyPercent);
        BtnSeventyFivePercent.onClick.AddListener(SeventyFivePercent);
        BtnTwentyFivePercent.onClick.AddListener(TwentyFivePercent);

	}

    void FiftyPercent() {
        Debug.Log("50");

        float goldAmount = GameInfo.totalGold * 0.50f;

        if (GameInfo.planet == "blue") {

            GameInfo.blueGold += goldAmount;

        }
        else if (GameInfo.planet == "red") {

            GameInfo.redGold += goldAmount;

        }
        else if (GameInfo.planet == "green") {

            GameInfo.greenGold += goldAmount;

        }
        else if (GameInfo.planet == "yellow") {

            GameInfo.yellowGold += goldAmount;

        }
        else if (GameInfo.planet == "pink") {

            GameInfo.pinkGold += goldAmount;

        }

        GameInfo.totalGold -= goldAmount;
        Menu.SetActive(false);

    }

    void TwentyFivePercent() {
        Debug.Log("25");

        float goldAmount = GameInfo.totalGold * 0.25f;

        if (GameInfo.planet == "blue") {

            GameInfo.blueGold += goldAmount;

        }
        else if (GameInfo.planet == "red") {

            GameInfo.redGold += goldAmount;

        }
        else if (GameInfo.planet == "green") {

            GameInfo.greenGold += goldAmount;

        }
        else if (GameInfo.planet == "yellow") {

            GameInfo.yellowGold += goldAmount;

        }
        else if (GameInfo.planet == "pink") {

            GameInfo.pinkGold += goldAmount;

        }

        GameInfo.totalGold -= goldAmount;
        Menu.SetActive(false);

    }

    void SeventyFivePercent() {
        Debug.Log("75");

        float goldAmount = GameInfo.totalGold * 0.75f;

        if (GameInfo.planet == "blue") {

            GameInfo.blueGold += goldAmount;

        }
        else if (GameInfo.planet == "red") {

            GameInfo.redGold += goldAmount;

        }
        else if (GameInfo.planet == "green") {

            GameInfo.greenGold += goldAmount;

        }
        else if (GameInfo.planet == "yellow") {

            GameInfo.yellowGold += goldAmount;

        }
        else if (GameInfo.planet == "pink") {

            GameInfo.pinkGold += goldAmount;

        }

        GameInfo.totalGold -= goldAmount;
        Menu.SetActive(false);

    }
	
}
