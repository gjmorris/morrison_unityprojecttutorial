﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AsteroidFieldCollision : MonoBehaviour {

    private void OnCollisionEnter(Collision collision) {

        if (collision.gameObject.CompareTag("Player")) {

            Destroy(gameObject);
            GameInfo.playerPos = collision.gameObject.transform.position;
            GameInfo.playerRotation = collision.gameObject.transform.rotation;
            SceneManager.LoadScene("MainGame");

        }

    }
}
